Doc Sphinx
==========

.. image:: https://drone.io/bitbucket.org/naglis/doc_sphinx/status.png
    :target: https://drone.io/bitbucket.org/naglis/doc_sphinx/latest

The module discovers modules with Sphinx documentation and builds one master
help which can then be accessed in a browser.

The build is triggered everytime the server is started.

Requirements
------------

* Open ERP 7
* Sphinx

How to get started
------------------
For your module's documentation to get picked-up by *Doc Sphinx*, make sure 
you have the following directory structure for your module.

::

  your_module/
    docs/
        index.rst
        conf.py
        some_page.rst

Use ``--log-level debug`` when starting Open ERP to see *Sphinx* build output.

Windows support
---------------
*Doc Sphinx* is based on symlinks, so it will not work on Windows.


