Welcome to Open ERP's documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 2

{modules:s}

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

