# -*- coding: utf-8 -*-

import imp
import logging
import os
import subprocess
import openerp
from openerp.osv import orm

logger = logging.getLogger(__name__)

SPHINX_CMD = 'sphinx-build'
# Python 3 is the default on Arch Linux
if os.path.exists('/etc/arch-release'):
    SPHINX_CMD = 'sphinx-build2'


def list_subdirs(dirname):
    """Returns the list of the subdirectories for a directory."""
    try:
        return next(os.walk(dirname))[1]
    except StopIteration:
        return []


def list_files(dirname):
    """Returns the list of filenames in the directory."""
    try:
        return next(os.walk(dirname))[2]
    except StopIteration:
        return []


def link(source_dir, dest_dir):
    """Creates a symlink of a directory.

    Ignores errors when the link already exists."""
    try:
        os.symlink(source_dir, dest_dir)
    except OSError as e:
        if not e.errno == os.errno.EEXIST:
            raise


def remove_links(directory):
    """Remove symlinked directories from :attr:`directory`."""

    for dir_name in list_subdirs(directory):
        full_path = os.path.join(directory, dir_name)
        if os.path.islink(full_path):
            os.remove(full_path)


class DocSphinx(orm.AbstractModel):

    _name = 'doc_sphinx'
    _register = True

    def init(self, cr):
        self.module_root = os.path.dirname(
            os.path.dirname(os.path.abspath(__file__)))
        self.docs_dir = os.path.join(self.module_root, '_docs')
        self.docs_source_dir = os.path.join(self.docs_dir, 'modules')
        self.verbose = openerp.tools.config['log_level'] == 'debug'
        addons_paths = openerp.tools.config['addons_path'].split(',')

        is_test_run = openerp.tools.config['test_enable']
        if is_test_run:
            return

        if not os.path.exists(self.docs_source_dir):
            try:
                os.makedirs(self.docs_source_dir)
            except OSError as e:
                raise orm.except_orm('OSError', str(e))

        model = self.pool.get('ir.module.module')
        installed_ids = model.search(cr, openerp.SUPERUSER_ID,
                                     [('state', '=', 'installed')],
                                     context={})
        installed = model.read(cr, openerp.SUPERUSER_ID,
                               installed_ids, fields=['name'])
        mod_names = [m['name'] for m in installed]
        # Remove previous symlinks.
        remove_links(self.docs_source_dir)
        master_docs = self.discover_addons_with_docs(mod_names, addons_paths)

        self.build_docs(master_docs)

    @staticmethod
    def find_addon_master_doc(module_name, module_path):
        """Returns the name of the master doc if the module has Sphinx docs.

            :attr:`module_name` - name of the module
            :attr:`module_path` - absolute path to the addon
        """
        if 'docs' not in list_subdirs(module_path):
            return

        files = list_files(os.path.join(module_path, 'docs'))
        if 'conf.py' in files:
            conf_fname = os.path.join(module_path, 'docs', 'conf.py')
            try:
                conf = imp.load_source('%s_conf' % module_name, conf_fname)
                return '%s%s' % (conf.master_doc, conf.source_suffix)
            except AttributeError:
                logger.debug('Missing attribute in %s' % conf_fname)
            except ImportError:
                pass
        elif 'index.rst' in files:
            return 'index.rst'

    def discover_addons_with_docs(self, installed, addon_paths):
        """Discover modules with documentation and return names of their
        master doc files."""
        master_docs = []
        installed = set(installed)
        for addon_path in addon_paths:
            modules = set(list_subdirs(addon_path))
            for mod_name in modules.intersection(installed):
                mod_path = os.path.join(addon_path, mod_name)
                master_doc = DocSphinx.find_addon_master_doc(mod_name, mod_path)
                if not master_doc:
                    continue
                docs_path = os.path.join(mod_path, 'docs')
                link(docs_path, os.path.join(self.docs_source_dir, mod_name))
                master_docs.append(os.path.join(
                    'modules', mod_name, master_doc))
        return master_docs

    def build_docs(self, master_docs):
        """Build documentation from links to master docs of other modules."""
        if not master_docs:
            logger.info('No modules with documentation found')
            return

        logger.info('Modules with documentation found: %d' % len(master_docs))
        template = open(os.path.join(self.docs_dir, 'index.tpl')).read()
        assert '{modules:s}' in template, 'Invalid template'
        with open(os.path.join(self.docs_dir, 'index.rst'), 'w') as f:
            f.write(template.format(
                modules='\n'.join(['   %s' % m for m in master_docs])))

        logger.info('Building master doc')
        stdout = not self.verbose and open(os.devnull, 'w')
        rv = subprocess.call(
            [
                'make',
                'html',
                'SPHINXBUILD=%s' % SPHINX_CMD,
                '--directory',
                self.docs_dir
            ], stdout=stdout)

        if rv != 0:
            if not self.verbose:
                logger.error('Failed to build the master documentation. '
                             'Run with --log-level debug to see build output.')
        else:
            # Create a link in the 'static' dir, so that the docs can be
            # accessed from a browser.
            link(os.path.join(self.docs_dir, '_build', 'html'),
                 os.path.join(self.module_root, 'static', 'docs'))
