# -*- coding: utf-8 -*-

from os import path

from openerp.addons.doc_sphinx.models.doc_sphinx import DocSphinx

import unittest2


class TestDocSphinx(unittest2.TestCase):

    def setUp(self):
        self.addons_path = path.join(path.dirname(path.abspath(__file__)),
                                     'test_modules')
        self.mod_name_1 = 'mod_with_docs_1'
        self.mod_name_2 = 'mod_with_docs_no_confpy'
        self.mod_name_3 = 'mod_with_docs_no_confpy_or_indexrst'
        self.mod_name_4 = 'mod_without_docs'

    def assert_master_doc(self, mod_name, expected):
        mod_path = path.join(self.addons_path, mod_name)
        master_doc = DocSphinx.find_addon_master_doc(mod_name, mod_path)
        self.assertEquals(expected, master_doc, 'Wrong master doc filename')

    def test_find_master_doc(self):
        self.assert_master_doc(self.mod_name_1, 'index.rst')

    def test_find_master_doc_no_confpy(self):
        self.assert_master_doc(self.mod_name_2, 'index.rst')

    def test_find_master_doc_no_confpy_or_index(self):
        self.assert_master_doc(self.mod_name_3, None)

    def test_find_master_doc_no_docs(self):
        self.assert_master_doc(self.mod_name_4, None)
