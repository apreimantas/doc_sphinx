openerp.doc_sphinx = function(instance) {
    instance.web.UserMenu.include({
        on_menu_help: function() {
            var self = this;
            if (!this.getParent().has_uncommitted_changes()) {
                var P = new instance.web.Model('ir.config_parameter');
                P.call('get_param', ['web.base.url']).then(function(base_url) {
                    window.open(base_url + '/doc_sphinx/static/docs/index.html', '_blank');
                });
            }
        },
    });
};
